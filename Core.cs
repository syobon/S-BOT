using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.Generic;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

namespace SBOT
{
    class Core
    {
        public DiscordClient Client { get; set; }
        public CommandsNextModule Commands { get; set; }
        public static Dictionary<string, int> exps = new Dictionary<string, int>();
        public static Random rnd = new Random();

        static List<ulong> noExp = new List<ulong>();

        static string lastAuthorId;

        public static void Main(string[] args)
        {
            noExp.Add(491194247559774218);
            noExp.Add(592657571920347136);
            noExp.Add(605634779148451843);
            noExp.Add(620939790002683934);
            noExp.Add(913351867797495818);
            noExp.Add(972475697090162788);
            noExp.Add(1056923442269650965);
            var prog = new Core();
            prog.MainAsync().GetAwaiter().GetResult();
        }

        public async Task MainAsync()
        {
            DiscordConfiguration clientConfig = new DiscordConfiguration
            {
                Token = System.Environment.GetEnvironmentVariable("TOKEN"),
                TokenType = TokenType.Bot,
                UseInternalLogHandler = true,
                LogLevel = LogLevel.Info
            };

            CommandsNextConfiguration commandsConfig = new CommandsNextConfiguration
            {
                StringPrefix = "?!",
                EnableMentionPrefix = true
            };

            this.Client = new DiscordClient(clientConfig);

            this.Client.Ready += this.Client_Ready;
            this.Client.GuildAvailable += this.Client_GuildAvailable;
            this.Client.ClientErrored += this.Client_ClientError;

            this.Commands = this.Client.UseCommandsNext(commandsConfig);

            this.Commands.CommandExecuted += this.Commands_CommandExecuted;
            this.Commands.CommandErrored += this.Commands_CommandErrored;

            this.Commands.RegisterCommands<BasicCommands>();

            this.Client.MessageCreated += async e =>
            {
                if (Regex.IsMatch(e.Message.Content, ".*(推し|おし)が(尊い|とうとい|てえてえ|てぇてぇ).*"))
                {
                    await e.Message.RespondWithFileAsync("/app/oshi.jpg");
                }
                if (e.Guild.Name == "SyoBAR" && !noExp.Contains(e.Channel.Id) && e.Channel.ParentId != 757895029674082336 && !(e.Author.IsBot))
                {
                    if (!(exps.ContainsKey(e.Author.Id.ToString())))
                    {
                        exps.Add(e.Author.Id.ToString(), 0);
                    }

                    // string message = Regex.Replace(e.Message.Content, @"\s", "");
                    string message = new string(e.Message.Content.ToCharArray().Distinct().ToArray());
                    // e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{message}", DateTime.Now);
                    if (e.Message.Content.StartsWith("?!") || message.Length == 0)
                    {
                        e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{e.Author.Username} couldn't get exp.", DateTime.Now);

                    }
                    else if (e.Author.Id.ToString() == lastAuthorId)
                    {
                        e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{e.Author.Username} couldn't get exp.", DateTime.Now);
                    }
                    else
                    {
                        lastAuthorId = e.Author.Id.ToString();
                        int oldLevel = (int)Math.Floor(Math.Sqrt(exps[e.Author.Id.ToString()] / 50));
                        int newLevel = 0;

                        if (e.Message.Content.StartsWith('>') || e.Message.Content.Contains("```") || e.Message.Content.Contains("://"))
                        {
                            exps[e.Author.Id.ToString()] += 5;
                            e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{e.Author.Username} got 5 fixed exp.", DateTime.Now);
                        }
                        else if (message.Length > 50)
                        {
                            exps[e.Author.Id.ToString()] += 50;
                            e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{e.Author.Username} got 50 fixed exp.", DateTime.Now);
                        }
                        else
                        {
                            exps[e.Author.Id.ToString()] += message.Length;
                            e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{e.Author.Username} got {message.Length} exp.", DateTime.Now);
                        }

                        newLevel = (int)Math.Floor(Math.Sqrt((exps[e.Author.Id.ToString()]) / 50));

                        if (newLevel > oldLevel)
                        {
                            var notification = e.Guild.GetChannel(535065379639394304);
                            await notification.SendMessageAsync($"【レベルアップ】\r{e.Author.Mention}\rレベル{oldLevel} → {newLevel}");
                        }

                        exps = exps.OrderByDescending(x => x.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
                        IniManager.Save("exp.ini", exps);

                        var channel = e.Guild.GetChannel(768362571173593131);
                        await channel.SendFileAsync("exp.ini");
                    }
                }
            };

            await this.Client.ConnectAsync();
            await Task.Delay(-1);
        }

        private Task Client_Ready(ReadyEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", "Client is ready to process events.", DateTime.Now);
            return Task.CompletedTask;
        }

        private Task Client_GuildAvailable(GuildCreateEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"Guild available: {e.Guild.Name}", DateTime.Now);
            if (e.Guild.Name == "SyoBAR")
            {
                var channel = e.Guild.GetChannel(768362571173593131);
                var messageTask = channel.GetMessageAsync(channel.LastMessageId);
                var message = messageTask.Result;
                var attach = message.Attachments;

                System.Net.WebClient wc = new System.Net.WebClient();
                wc.DownloadFile(attach[0].Url, @"./exp.ini");
                wc.Dispose();

                exps = IniManager.Load("exp.ini");
            }
            return Task.CompletedTask;
        }

        private Task Client_ClientError(ClientErrorEventArgs e)
        {
            e.Client.DebugLogger.LogMessage(LogLevel.Error, "S-BOT", $"Exception occured: {e.Exception.GetType()}: {e.Exception.Message}", DateTime.Now);
            return Task.CompletedTask;
        }

        private Task Commands_CommandExecuted(CommandExecutionEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Info, "S-BOT", $"{e.Context.User.Username} successfully executed '{e.Command.QualifiedName}'", DateTime.Now);
            return Task.CompletedTask;
        }

        private async Task Commands_CommandErrored(CommandErrorEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Error, "S-BOT", $"{e.Context.User.Username} tried executing '{e.Command?.QualifiedName ?? "<unknown command>"}' but it errored: {e.Exception.GetType()}: {e.Exception.Message ?? "<no message>"}", DateTime.Now);

            if (!(e.Exception is CommandNotFoundException exa))
            {
                if (e.Exception is ChecksFailedException exb)
                {
                    var emoji = DiscordEmoji.FromName(e.Context.Client, ":no_entry:");

                    var embed = new DiscordEmbedBuilder
                    {
                        Title = "失敗",
                        Description = $"{emoji} コマンドを実行する権限がないか、クールダウン中です。",
                        Color = new DiscordColor(0xFF0000)
                    };
                    await e.Context.RespondAsync("", embed: embed);
                }
                else
                {
                    await e.Context.RespondAsync($"エラーが発生しました。\r```\r{e.Exception.GetType()}: {e.Exception.Message ?? "<no message>"}\r```");
                }
            }
        }
    }
}
